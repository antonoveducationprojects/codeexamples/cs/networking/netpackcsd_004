﻿/*
 * Created by SharpDevelop.
 * User: Yuriy
 * Date: 22.10.2018
 * Time: 23:04
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;

namespace NetClient
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Net Client is starting ....\nWait please ..........");
			try
            {
				Console.Write("Server listen IP: ");
                string serverIP= Console.ReadLine();
			    IPAddress ipAddr = IPAddress.Parse(serverIP);//ipHost.AddressList[0];
                Console.Write("Server listen Port: ");			
                string serverPort= Console.ReadLine();                
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddr, int.Parse(serverPort));
                //connect ???
                TcpSender sender = new TcpSender(ipAddr, ipEndPoint);
                SendMessage(sender);
                //disconnect ???
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
            	Console.Write("Client is stopped \nPress any key to continue . . . ");
			    Console.ReadKey(true);
            }
        }
        static void SendMessage(/*TcpSender=>*/ IMsgSender sender)
        {
        	//1. Prepare to trasfer message
            Console.Write("Input message: ");
            string message = Console.ReadLine();
            //2. Sending message
            sender.Send(message);
            //Console.WriteLine("Strarting conection to {0} ", sender.RemoteEndPoint.ToString());
            string response = sender.Receive();
            // int bytesRec = sender.Receive(bytes);
            Console.WriteLine("\nResponse: {0}\n\n", response);
            //3. Close connection
            //???			            
            //4. Check for exit
            if (message.IndexOf("<TheEnd>") == -1)
                SendMessage(sender);
		}
	}
}