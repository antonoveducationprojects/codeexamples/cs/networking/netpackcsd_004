namespace NetClient;

public interface IMsgSender
{
    int Send(string message);
    string Receive();
}